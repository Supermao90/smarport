package smartport.syaywa.com.smarport.NewTrip;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import smartport.syaywa.com.smarport.R;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class OverviewFragment extends BaseFragment
{
    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        mActivity = (NewTripFragmentActivity) activity;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mActivity = null;
    }

    private ImageView mImageView;
    private TextView mTextView;
    private WebView mWebView;

    private LinearLayout mMiddler0;
    private LinearLayout mMiddler1;
    private LinearLayout mMiddler2;
    private LinearLayout mMiddler3;
    private TextView mCurrency;
    private NewTripFragmentActivity mActivity;

    private static String URL = "URL";
    private static String WEB_VIEW = "WEB_VIEW";
    private static String CURRENCY_URL = "http://qq.ip138.com/hl.asp?from=USD&to=CNY&q=1";
    private static String CURRENCY_KEY = "CURRENCY_KEY";

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private String mCurrencyText;
    private SharedPreferences mPrf;
    private static String CACHED_CURRENCY = "6.2074";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        mPrf = this.getActivity().getSharedPreferences(
                this.getActivity().getPackageName(), Context.MODE_PRIVATE);
        final ViewGroup rootViewOverview =
                (ViewGroup) inflater.inflate(R.layout.layout_fragment_visa_overview, container,
                        false);
        mWebView = (WebView) rootViewOverview.findViewById(R.id.wbv_overview);
//        mWebView.getSettings().setAppCachePath(getActivity().getApplicationContext().getCacheDir().getAbsolutePath());
//        mWebView.getSettings().setAllowFileAccess(true);
//        mWebView.getSettings().setAppCacheEnabled(true);
//        mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
//        if ( !isNetworkAvailable() ) { // loading offline
//            mWebView.getSettings().setCacheMode( WebSettings.LOAD_CACHE_ELSE_NETWORK );
//        }
//        mWebView.setOnTouchListener(new View.OnTouchListener()
//        {
//            @Override
//            public boolean onTouch(View v, MotionEvent event)
//            {
//                return (event.getAction() == MotionEvent.ACTION_MOVE);
//            }
//        });
//
//        mWebView.setWebViewClient(new WebViewClient(){
//            @Override
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
//            {
//                mWebView.loadUrl("file:///android_asset/overview.html");
//                mWebView.getSettings().setJavaScriptEnabled(false);
////                Toast.makeText(OverviewFragment.this.getActivity(), "请检查您的网络 " + description , Toast.LENGTH_SHORT).show();
//                super.onReceivedError(view, errorCode, description, failingUrl);
//            }
//        });
//        mWebView.loadUrl("http://visaapplicationtext.weebly.com");

        mWebView.loadUrl("file:///android_asset/overview.html");
        mWebView.getSettings().setJavaScriptEnabled(false);

        mMiddler0 = (LinearLayout) rootViewOverview.findViewById(R.id.middler_e0);
        mMiddler1 = (LinearLayout) rootViewOverview.findViewById(R.id.middler_e1);
        mMiddler2 = (LinearLayout) rootViewOverview.findViewById(R.id.middler_e2);
        mMiddler3 = (LinearLayout) rootViewOverview.findViewById(R.id.middler_e3);

        //0
        TextView footerWidget0Title = (TextView) mMiddler0.findViewById(R.id.footer_widget_text_above);
        ImageView footerWidget0Image = (ImageView) mMiddler0.findViewById(R.id.footer_widget_image_above);
        footerWidget0Image.setVisibility(View.GONE);
        footerWidget0Title.setVisibility(View.VISIBLE);

        TextView footerWidget0Text = (TextView) mMiddler0.getChildAt(1);
        footerWidget0Text.setText("实时汇率");
        mCurrency = footerWidget0Title;
        mMiddler0.setOnClickListener(openWebviewButtonClickListener("http://qianzhenghuilv.weebly.com"));
        if (mCurrencyText == null || mCurrencyText == CACHED_CURRENCY)
        {
            getCurrency();
        }
        else
        {
            mCurrency.setText(mCurrencyText);
        }


        //1
        TextView footerWidget1Title = (TextView) mMiddler1.findViewById(R.id.footer_widget_text_above);
        ImageView footerWidget1Image = (ImageView) mMiddler1.findViewById(R.id.footer_widget_image_above);
        footerWidget1Image.setVisibility(View.GONE);
        footerWidget1Title.setVisibility(View.VISIBLE);
        footerWidget1Title.setText("$160");

        TextView footerWidget1Text = (TextView) mMiddler1.getChildAt(1);
        footerWidget1Text.setText("签证费用");
        mMiddler1.setOnClickListener(
                openWebviewButtonClickListener("http://www.ustraveldocs.com/cn_zh/cn-niv-visafeeinfo.asp"));

        //2
        ImageView footerWidget2Title = (ImageView) mMiddler2.findViewById(R.id.footer_widget_image_above);
        footerWidget2Title.setImageDrawable(getResources().getDrawable(R.drawable.bag));

        TextView footerWidget2Text = (TextView) mMiddler2.getChildAt(1);
        footerWidget2Text.setText("热门景点");
        mMiddler2.setOnClickListener(openWebviewButtonClickListener("http://remenjingdian.weebly.com/"));

        //3
        ImageView footerWidget3Title = (ImageView) mMiddler3.findViewById(R.id.footer_widget_image_above);
        footerWidget3Title.setImageDrawable(getResources().getDrawable(R.drawable.snow));

        TextView footerWidget3Text = (TextView) mMiddler3.getChildAt(1);
        footerWidget3Text.setText("气候特征");
        mMiddler3.setOnClickListener(openWebviewButtonClickListener("http://qihou.weebly.com/"));

//        //4
//        ImageView footerWidget4Title = (ImageView) mFooterWidget4.findViewById(R.id.middler_e4);
//        footerWidget4Title.setImageDrawable(getResources().getDrawable(R.drawable.more));
//
//        TextView footerWidget4Text = (TextView) mFooterWidget4.getChildAt(1);
//        footerWidget4Text.setText("More");

//        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.content_frame);
//        ViewTreeObserver vto = frameLayout.getViewTreeObserver();
//        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
//        {
//            @Override
//            public void onGlobalLayout()
//            {
//                frameLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                mFrameLayoutHeight = frameLayout.getMeasuredHeight();
//            }
//        });

        return rootViewOverview;
    }

    //TODO: put this method to util
    private boolean isNetworkAvailable()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.getActivity().getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private class DownloadWebPageTask extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {
            for (String url : urls)
            {
                long start = System.currentTimeMillis();
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);

                try
                {
                    HttpResponse execute = client.execute(httpGet);
                    InputStream content = execute.getEntity().getContent();

                    BufferedReader buffer = new BufferedReader(
                            new InputStreamReader(content));
                    String s;
                    while ((s = buffer.readLine()) != null)
                    {
                        if (s.startsWith("<tr bgcolor=#ffffff align=center><td>1</td><td>"))
                        {
                            s = s.replaceAll("<td>", "Wensheng");
                            s = s.replaceAll("</td>", "Wensheng");
                            String[] strs = s.split("Wensheng");
                            for (String str : strs)
                            {
                                if (str.matches("\\d.\\d*"))
                                {
                                    return str;
                                }
                            }
                        }
                    }

                }
                catch (Throwable e)
                {
                    System.out.println("Wensheng Loading currency ERROR " + e);
                    e.printStackTrace();
                    return CACHED_CURRENCY;
                }
            }
            return "Wensheng";
        }

        @Override
        protected void onPostExecute(String result)
        {
            if (result != CACHED_CURRENCY)
            {
                if (!mPrf.edit().putString(CURRENCY_KEY, result).commit())
                {
                    System.out.println("Wensheng save currency failed " + result);
                }
            }
            else
            {
                result = mPrf.getString(CURRENCY_KEY, CACHED_CURRENCY);
            }

            mCurrency.setText(result);
            mCurrencyText = result;
        }
    }

    private void getCurrency()
    {
        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute(CURRENCY_URL);

    }

    private View.OnClickListener openWebviewButtonClickListener(final String url)
    {
        View.OnClickListener listener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                boolean isConnected = false;

                final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                isConnected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
                if(!isConnected){
                    Toast.makeText(getActivity(), "请检查您的网络连接。", Toast.LENGTH_SHORT).show();
                    return;
                }
                mActivity.hideFooterAndBack();
                WebViewFragment fragment = new WebViewFragment();
                Bundle b = new Bundle();
                b.putString(URL, url);
                fragment.setArguments(b);
                mFragmentTransaction = OverviewFragment.this.getActivity().getFragmentManager().beginTransaction();
                mFragmentTransaction.replace(R.id.content_frame, fragment, WEB_VIEW);
                mFragmentTransaction.addToBackStack(WEB_VIEW);
                mFragmentTransaction.commit();
            }
        };
        return listener;
    }
}
