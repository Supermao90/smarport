package smartport.syaywa.com.smarport.NewTrip;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.app.Activity;
import android.app.Fragment;
import android.view.View;

public class BaseFragment extends Fragment
{

    private OnFragmentInteractionListener mListener;


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            mListener = (OnFragmentInteractionListener) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener
    {
        public void onMessageReceiveFromFragment(String message);
    }

    @Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim)
    {
        Animator anim = super.onCreateAnimator(transit, enter, nextAnim);

        if (anim == null && nextAnim != 0)
        {
            anim = AnimatorInflater.loadAnimator(getActivity(), nextAnim);
        }

        if (anim != null)
        {
            getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

            anim.addListener(new Animator.AnimatorListener()
            {
                @Override
                public void onAnimationStart(Animator animation)
                {
                    mListener.onMessageReceiveFromFragment(Util.FRAGMENT_ANIMATION_START);
                }

                @Override
                public void onAnimationEnd(Animator animation)
                {
                    mListener.onMessageReceiveFromFragment(Util.FRAGMENT_ANIMATION_END);
                    if (getView() != null)
                    {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                }

                @Override
                public void onAnimationCancel(Animator animation)
                {
                    if (getView() != null)
                    {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }
                }

                @Override
                public void onAnimationRepeat(Animator animation)
                {
                }
            });
        }
        return anim;
    }

}
