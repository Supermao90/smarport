package smartport.syaywa.com.smarport.NewTrip;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import com.baidu.lbsapi.BMapManager;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.nplatform.comapi.map.MapController;
import smartport.syaywa.com.smarport.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by wenshenm on 3/29/15.
 */
public class AmbassyLocationFragment extends BaseFragment
{

    static final LatLng BEIJING = new LatLng(39.960891, 116.474269);
    static final LatLng CHENGDU = new LatLng(30.630943, 104.075071);
    static final LatLng GUANGZHOU = new LatLng(23.122699, 113.327037);
    static final LatLng SHANGHAI = new LatLng(31.23569, 121.463501);
    static final LatLng SHENYANG = new LatLng(41.789037, 123.433246);


    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private static LatLng[] locations = new LatLng[]{BEIJING, CHENGDU, SHANGHAI, GUANGZHOU, SHENYANG};


    MapView mapView;
    BaiduMap map;
//    GoogleMap map;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        try
        {
            SDKInitializer.initialize(this.getActivity().getApplicationContext());
        }
        catch (Exception e)
        {
            System.out.println("Wensheng map init error");
        }

        prepareListData();
        View v = inflater.inflate(R.layout.layout_ambassy_location, container, false);

        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) v.findViewById(R.id.mapview);
        expListView = (ExpandableListView) v.findViewById(R.id.lvExp_ambassy);

//        // Gets to GoogleMap from the MapView and does initialization stuff
        map = mapView.getMap();
//        map.getUiSettings().setMyLocationButtonEnabled(false);
//        map.setMyLocationEnabled(true);

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
//        try
//        {
//            MapsInitializer.initialize(this.getActivity());
//        }
//        catch (Exception e)
//        {
//            System.out.println("Wensheng map init error");
//        }
        return v;
    }

    private void prepareListData()
    {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("美国驻华大使馆（北京）");
        listDataHeader.add("美国驻华大使馆（成都）");
        listDataHeader.add("美国驻华大使馆（广州）");
        listDataHeader.add("美国驻华大使馆（上海）");
        listDataHeader.add("美国驻华大使馆（沈阳）");

        // Adding child data
        List<String> beijing = new ArrayList<String>();
        beijing.add("签证面谈在两个地点进行，请查看预约信，确定您的面谈地点。\n" +
                "新馆地址： 北京安家楼路55号，邮编：100600\n" +
                "日坛分部： 北京建国门外秀水东街2号，邮编：100600\n" +
                "•\t北京本地拨打： 56794700\n" +
                "•\t非北京本地拨打： 010-56794700\n");

        List<String> chengdu = new ArrayList<String>();
        chengdu.add("美国驻华总领事馆（成都）\n" +
                "四川成都领事馆路4号，邮编：610041\n" +
                "•\t成都本地拨打： 6273-6100\n" +
                "•\t非成都本地拨打： 028-6273-6100\n");

        List<String> guangzhou = new ArrayList<String>();
        guangzhou.add("美国驻华总领事馆（广州）\n" +
                "广州市天河区珠江新城华夏路,（靠近地铁3号线或5号线珠江新城站B1出口)\n" +
                "•\t广州本地拨打： 83909000\n" +
                "•\t非广州本地拨打： 020-83909000\n");

        List<String> shanghai = new ArrayList<String>();
        shanghai.add("美国驻华总领事馆（上海）\n" +
                "上海南京西路1038号梅龙镇广场8楼，邮编：200041\n" +
                "•\t上海本地拨打： 5191-5200 \n" +
                "•\t非上海本地拨打： (+86) 021-5191-5200\n");

        List<String> shenyang = new ArrayList<String>();
        shenyang.add("美国驻华总领事馆（沈阳）\n" +
                "沈阳和平区十四纬路52号，邮编：110003\n" +
                "•\t沈阳本地拨打： 31663400\n" +
                "•\t非沈阳本地拨打： 024-31663400\n");


        listDataChild.put(listDataHeader.get(0), beijing);
        listDataChild.put(listDataHeader.get(1), chengdu);
        listDataChild.put(listDataHeader.get(2), guangzhou);
        listDataChild.put(listDataHeader.get(3), shanghai);
        listDataChild.put(listDataHeader.get(4), shenyang);
    }

    //BMapManager 对象管理地图、定位、搜索功能
    private BMapManager mBMapManager;
    private MapController mMapController = null;  //地图控制

    @Override
    public void onResume()
    {
        super.onResume();
        mapView.onResume();

        BitmapDescriptor bitmap = BitmapDescriptorFactory
                .fromResource(R.drawable.pin);
//构建MarkerOption，用于在地图上添加Marker
        OverlayOptions option = new MarkerOptions()
                .position(BEIJING)
                .icon(bitmap);
//在地图上添加Marker，并显示
        map.addOverlay(option);

        MapStatus mMapStatus = new MapStatus.Builder()
                .target(BEIJING)
                .zoom(12)
                .build();

        MapStatusUpdate mMapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mMapStatus);
//改变地图状态
        map.setMapStatus(mMapStatusUpdate);

        // Updates the location and zoom of the MapView
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(BEIJING, 15);
//        map.animateCamera(cameraUpdate);
//
//        map.addMarker(new MarkerOptions()
//                .position(BEIJING)
//                .title("Embassy of the United States, Beijing"));
//
//        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener()
//        {
//
//            private float currentZoom = -1;
//
//            @Override
//            public void onCameraChange(CameraPosition position)
//            {
//                System.out.println("Wensheng check zoom " + position.zoom);
////                if (position.zoom != currentZoom)
////                {
////                    currentZoom = position.zoom;  // here you get zoom level
////                }
//            }
//        });

        listAdapter = new ExpandableListAdapter(this.getActivity(), listDataHeader, listDataChild, false, null);
        expListView.setAdapter(listAdapter);
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener()
        {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id)
            {
                System.out.println("Wensheng position on itemClick");

                BitmapDescriptor bitmap = BitmapDescriptorFactory
                        .fromResource(R.drawable.pin);
//构建MarkerOption，用于在地图上添加Marker
                OverlayOptions option = new MarkerOptions()
                        .position(locations[groupPosition])
                        .icon(bitmap);
//在地图上添加Marker，并显示
                map.addOverlay(option);

                MapStatus mMapStatus = new MapStatus.Builder()
                        .target(locations[groupPosition])
                        .zoom(12)
                        .build();

                MapStatusUpdate mMapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mMapStatus);
//改变地图状态
                map.setMapStatus(mMapStatusUpdate);
                return false;
            }
        });
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        mapView.onPause();
    }

//    @Override
//    public void onLowMemory(){
//        super.onLowMemory();
//        mapView.onLowMemory();
//    }
}
