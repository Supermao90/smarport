package smartport.syaywa.com.smarport.NewTrip;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import smartport.syaywa.com.smarport.R;

/**
 * Created by wenshenm on 4/13/15.
 */
public class ExploreListViewArrayAdapter extends ArrayAdapter<String>
{
    public static final String PRODUCTS[] = {"遨游", "自由", "异域"};
    private static final String NOTHING[] = {"更多"};
    private static final String USA_AND_NOTHING[] = {"美国", "更多"};
    public static int[] PRODUCTS_DRWABLE_SRC =
            {R.drawable.explore_italy, R.drawable.explore_usa, R.drawable.explore_thailand};
    private int[] mDrawables;
    private Context mContext;
    private boolean mIsBannerShown = false;
    private int mApplicationStatus = 0;
    private static int  mMoreIndex = 0;

    public static int getMoreIndex(){
        return mMoreIndex;
    }

    public ExploreListViewArrayAdapter(Context context, int resource, int textViewResourceId, boolean isBannerShown,
                                       int applicationStatus)
    {
        super(context, resource, textViewResourceId, itemsSelection(applicationStatus));

        mDrawables = PRODUCTS_DRWABLE_SRC;
        mContext = context;
        mIsBannerShown = isBannerShown;
        mApplicationStatus = applicationStatus;
    }


    public static String[] itemsSelection(int i)
    {
        if (i == 0)
        {
            return PRODUCTS;
        }

        if (i == 1)
        {
            mMoreIndex = 0;
            return NOTHING;
        }

        else
        {
            mMoreIndex = 1;
            return USA_AND_NOTHING;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = super.getView(position, convertView, parent);
        if (!mIsBannerShown)
        {
            ImageView im = (ImageView) v.findViewById(R.id.explore_imageview);
            im.setImageDrawable(mContext.getResources().getDrawable(mDrawables[position]));
            LinearLayout exploreListViewItemBanner = (LinearLayout) v.findViewById(R.id.explore_list_item_banner);
            exploreListViewItemBanner.setVisibility(View.GONE);
        }
        else
        {
            if (mApplicationStatus == 1)
            {
                LayoutInflater inflater =
                        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                RelativeLayout layout =
                        (RelativeLayout) inflater.inflate(R.layout.explore_list_view_item_add_new, null);
                return layout;
            }
            else
            {
                if (position == 0)
                {
                    ImageView im = (ImageView) v.findViewById(R.id.explore_imageview);
                    im.setImageDrawable(mContext.getResources().getDrawable(mDrawables[1]));
                    TextView tv = (TextView) v.findViewById(R.id.explore_country_name);
                    tv.setVisibility(View.GONE);
                    LinearLayout exploreListViewItemBanner =
                            (LinearLayout) v.findViewById(R.id.explore_list_item_banner);
                    exploreListViewItemBanner.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            Intent i = new Intent(mContext, NewTripFragmentActivity.class);
                            i.putExtra(Util.DOCS_PREP_KEY, true);
                            mContext.startActivity(i);
                        }
                    });
                    TextView tv1 = (TextView) v.findViewById(R.id.go_to_new_trip_text);
                    TextView tv2 = (TextView) v.findViewById(R.id.visa_pre_status);
                    if (mApplicationStatus == 2)
                    {
                        tv1.setText("申请中");
                        tv2.setText("未完成");
                    }
                    else
                    {
                        tv1.setText("查看历程");
                        tv2.setText("已完成");
                    }
                }
                else
                {
                    LayoutInflater inflater =
                            (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    RelativeLayout layout =
                            (RelativeLayout) inflater.inflate(R.layout.explore_list_view_item_add_new, null);
                    return layout;
                }

            }
        }
        return v;
    }
}
