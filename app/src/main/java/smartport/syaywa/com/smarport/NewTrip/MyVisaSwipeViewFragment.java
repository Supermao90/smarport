package smartport.syaywa.com.smarport.NewTrip;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.util.Attributes;
import smartport.syaywa.com.smarport.R;

/**
 * Created by wenshenm on 4/5/15.
 */
public class MyVisaSwipeViewFragment extends BaseFragment
{
    private ListView mListView;
    private MyVisaListAdapter mAdapter;
    private Context mContext = getActivity();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.layout_fragment_my_visa, container, false);
        mListView = (ListView) v.findViewById(R.id.my_visa_list_view);
        mAdapter = new MyVisaListAdapter(this.getActivity());
        mListView.setAdapter(mAdapter);
        mAdapter.setMode(Attributes.Mode.Single);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ((SwipeLayout) (mListView.getChildAt(position - mListView.getFirstVisiblePosition()))).open(true);
            }
        });
        mListView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                Log.e("Wensheng", "OnTouch " + event.getAction());
                return false;
            }
        });
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                Toast.makeText(mContext, "OnItemLongClickListener", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        mListView.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState)
            {
                Log.e("Wensheng", "onScrollStateChanged " + scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
            {
                Log.e("Wensheng", "onScroll " + firstVisibleItem + " " + visibleItemCount + " " + totalItemCount);
            }
        });

        mListView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                System.out.println("Wensheng " + position + " " + id + " " + "selected");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                System.out.println("Wensheng nothing is selected");
            }
        });

        return v;
    }
}
