/*
 * Copyright 2011 woozzu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartport.syaywa.com.smarport.NewTrip;

import com.github.stuxuhai.jpinyin.PinyinHelper;

public class StringMatcher
{

    private final static char CHINESE_UNICODE_START = 0x4e00;
    private final static char CHINESE_UNICODE_END = 0x9fbb;

    public static boolean match(String value, String keyword)
    {
        if (value == null || keyword == null)
        {
            return false;
        }
        if (keyword.length() > value.length())
        {
            return false;
        }

        if (isChinese(value.charAt(0)) && isChinese(keyword.charAt(0)))
        {

            value = PinyinHelper.getShortPinyin(value);
            keyword = PinyinHelper.getShortPinyin(keyword);
        }

        int i = 0, j = 0;
//        do
//        {
//            if (keyword.charAt(j) == value.charAt(i))
//            {
//                i++;
//                j++;
//            }
//            else if (j > 0)
//            {
//                break;
//            }
//            else
//            {
//                i++;
//            }
//        } while (i < value.length() && j < keyword.length());
//
        while(i < value.length() && j < keyword.length()){
            if (keyword.charAt(j) == value.charAt(i))
            {
                i++;
                j++;
            }
            else if (j > 0)
            {
                break;
            }
            else
            {
                i++;
            }
        }
        return (j == keyword.length()) ? true : false;
    }

    private static boolean isChinese(char c)
    {
        if (c >= CHINESE_UNICODE_START && c <= CHINESE_UNICODE_END)
        {
            return true;
        }
        return false;
    }
}
