package smartport.syaywa.com.smarport.NewTrip;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import smartport.syaywa.com.smarport.R;

/**
 * Created by wenshenm on 4/16/15.
 */
public class CountryListActivity extends Activity
{
    private EditText mInputSearch;

    private TextView mFooterTextView1;
    private TextView mFooterTextView2;
    private TextView mFooterTextView3;

    @Override
    protected void onResume()
    {
//        getWindow().getDecorView()
//                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onResume();

        if (mFooterTextView1 != null)
        {
            mFooterTextView1.setSelected(false);
            mFooterTextView2.setSelected(true);
            mFooterTextView3.setSelected(false);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_explore);

        mInputSearch = (EditText) findViewById(R.id.explore_inputSearch);

        mInputSearch.addTextChangedListener(new TextWatcher()
        {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3)
            {
                // When user changed the Text
//                MyVisaActivity.this.mListViewAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().length() > 0)
                {
                    mInputSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
                else
                {
                    //Assign your image again to the view, otherwise it will always be gone even if the text is 0 again.
                    mInputSearch.setCompoundDrawablesWithIntrinsicBounds(R.drawable.search_icon, 0, 0, 0);
                }
            }
        });

        mFooterTextView1 = (TextView) findViewById(R.id.footer_text_1);
        mFooterTextView2 = (TextView) findViewById(R.id.footer_text_2);
        mFooterTextView3 = (TextView) findViewById(R.id.footer_text_3);

        mFooterTextView2.setSelected(true);

        mFooterTextView1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mFooterTextView1.setSelected(false);
                v.setSelected(true);
                Intent intent = new Intent(CountryListActivity.this, ExploreActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            }
        });

        mFooterTextView3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mFooterTextView1.setSelected(false);
                v.setSelected(true);
                Intent intent = new Intent(CountryListActivity.this, NewTripFragmentActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

    }
}
