package smartport.syaywa.com.smarport.NewTrip;

/**
 * Created by wenshenm on 4/8/15.
 */

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

public class DotViewPager extends RelativeLayout implements
        OnPageChangeListener, Runnable
{
    private boolean isAutoNext = false;
    private int autoNextTime = 5000;
    private Handler hd = new Handler();
    private Context context;
    private ViewPager viewPager;
    private int count;
    private int tagImageId_seleced, tagImageId_nomorl;
    private int size = 200;
    private int margin = 5;
    private LinearLayout tagImageLayout;
    private List<ImageView> imageList = new ArrayList<ImageView>();
    private int marginButtom = 20;
    private int currentItem = 0;

    public DotViewPager(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;
    }

    public DotViewPager(Context context)
    {
        super(context);
        this.context = context;
    }

    public void init(int id1, int id2)
    {
        this.tagImageId_seleced = id1;
        this.tagImageId_nomorl = id2;
        tagImageLayout = new LinearLayout(context);
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.setMargins(0, 0, 0, marginButtom);
        tagImageLayout.setLayoutParams(params);
        this.addView(tagImageLayout);
        tagImageLayout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        tagImageLayout.setBottom(marginButtom);
        viewPager = new ViewPager(context);
        viewPager.setOnPageChangeListener(this);
        viewPager.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
        this.addView(viewPager);
    }

    public void init(int id1, int id2, int size, int imageMargin, int gravity,
                     int layoutMargin)
    {
        this.tagImageId_seleced = id1;
        this.tagImageId_nomorl = id2;
        this.margin = imageMargin;
        this.size = size;
        this.marginButtom = layoutMargin;
        tagImageLayout = new LinearLayout(context);
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        if (gravity == 2)
        {
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.setMargins(0, 0, 0, marginButtom);
        }
        else
        {
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params.setMargins(0, marginButtom, 0, 0);
        }
        tagImageLayout.setLayoutParams(params);
        addView(tagImageLayout);
        viewPager = new ViewPager(context);
        viewPager.setOnPageChangeListener(this);
        viewPager.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
        this.addView(viewPager);
    }

    public void setAdapter(int count)
    {
        this.count = count;
        viewPager.setAdapter(new MyPagerAdapter());
        initTagImage(this.count);
        if (isAutoNext)
        {
            hd.postDelayed(this, autoNextTime);
        }
    }

    public void notifyChanged(int count)
    {
        viewPager.getAdapter().notifyDataSetChanged();
        initTagImage(count);
    }

    private void initTagImage(int count2)
    {
        tagImageLayout.removeAllViews();
        imageList.clear();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size,
                size);
        params.setMargins(margin, 0, margin, 0);
        for (int i = 0; i < count2; i++)
        {
            ImageView view1 = new ImageView(context);
            view1.setLayoutParams(params);
            if (i == 0)
            {
                view1.setImageResource(tagImageId_seleced);
            }
            else
            {
                view1.setImageResource(tagImageId_nomorl);
            }
            view1.setScaleType(ScaleType.FIT_XY);
            imageList.add(view1);
            tagImageLayout.addView(view1);
        }
        tagImageLayout.bringToFront();
    }

    public void setAutoNext(boolean isAutoNext, int time)
    {
        this.isAutoNext = isAutoNext;
        this.autoNextTime = time;
    }

    @Override
    public void onPageScrollStateChanged(int arg0)
    {
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {
    }

    @Override
    public void onPageSelected(int arg0)
    {
        if (imageList.isEmpty())
        {
            return;
        }
        if (!isAutoNext)
        {
            for (int i = 0; i < imageList.size(); i++)
            {
                if (i == arg0)
                {
                    imageList.get(arg0).setImageResource(tagImageId_seleced);
                }
                else
                {
                    imageList.get(i).setImageResource(tagImageId_nomorl);
                }
            }
        }
        else
        {
            imageList.get(currentItem).setImageResource(tagImageId_nomorl);
            currentItem = arg0 % this.count;
            imageList.get(currentItem).setImageResource(tagImageId_seleced);
            hd.removeCallbacks(this);
            hd.postDelayed(this, autoNextTime);
        }

    }

    @Override
    public void run()
    {
        int count = viewPager.getCurrentItem();
        imageList.get(currentItem).setImageResource(tagImageId_nomorl);
        viewPager.setCurrentItem(++count);
        currentItem = viewPager.getCurrentItem() % this.count;
        imageList.get(currentItem).setImageResource(tagImageId_seleced);
        hd.postDelayed(this, autoNextTime);
    }

    OnGetView onGetView;

    public void setOnGetView(OnGetView onGetView)
    {
        this.onGetView = onGetView;
    }

    public interface OnGetView
    {
        public View getView(ViewGroup container, int position);
    }

    public class MyPagerAdapter extends PagerAdapter
    {

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            return onGetView.getView(container, position % count);
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }

        @Override
        public int getCount()
        {
            if (isAutoNext && count > 1)
            {
                return Integer.MAX_VALUE;
            }
            else
            {
                return count;
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position,
                                Object object)
        {
            container.removeView((View) object);
            object = null;
        }
    }

}
