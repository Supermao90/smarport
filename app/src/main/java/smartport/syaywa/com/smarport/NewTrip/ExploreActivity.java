package smartport.syaywa.com.smarport.NewTrip;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import smartport.syaywa.com.smarport.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wenshenm on 4/10/15.
 */
public class ExploreActivity extends Activity
{
    private ListView mListView;
    private ExploreListViewArrayAdapter mListViewAdapter;
    private EditText mInputSearch;

    private TextView mFooterTextView1;
    private TextView mFooterTextView2;
    private TextView mFooterTextView3;
    public final static String SEARCH = "SEARCH";

    @Override
    protected void onResume()
    {
//        getWindow().getDecorView()
//                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onResume();
        if (mFooterTextView1 != null)
        {
            mFooterTextView1.setSelected(true);
            mFooterTextView2.setSelected(false);
            mFooterTextView3.setSelected(false);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        ParseObject testObject = new ParseObject("TestObject");
        testObject.put("foo", "bar");
        testObject.saveInBackground();


        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        ParseAnalytics.trackAppOpenedInBackground(getIntent(), new SaveCallback()
        {
            @Override
            public void done(ParseException e)
            {
                System.out.println("Wnesheng track open " + e);
            }
        });


        Map<String, String> dimensions = new HashMap<String, String>();
// What type of news is this?
        dimensions.put("category", "politics");
// Is it a weekday or the weekend?
        dimensions.put("dayType", "weekday");
// Send the dimensions to Parse along with the 'read' event

        ParseAnalytics.trackEventInBackground("read", dimensions);

        setContentView(R.layout.activity_explore);

        mListView = (ListView) findViewById(R.id.explore_list_view);
        mInputSearch = (EditText) findViewById(R.id.explore_inputSearch);

        mListViewAdapter =
                new ExploreListViewArrayAdapter(this, R.layout.explore_list_view_item, R.id.explore_country_name, false, 0);
        mListView.setAdapter(mListViewAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            Intent intent = new Intent();
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                if(ExploreListViewArrayAdapter.PRODUCTS[position].contains("自由")){
                    intent.setClass(ExploreActivity.this, NewTripFragmentActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(ExploreActivity.this, "Coming soon.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mInputSearch.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if (hasFocus)
                {
                    Intent intent = new Intent(ExploreActivity.this, NewTripGenralActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra(ExploreActivity.SEARCH, true);
                    startActivity(intent);
                }
            }
        });

        mFooterTextView1 = (TextView) findViewById(R.id.footer_text_1);
        mFooterTextView2 = (TextView) findViewById(R.id.footer_text_2);
        mFooterTextView3 = (TextView) findViewById(R.id.footer_text_3);

        mFooterTextView1.setSelected(true);

        mFooterTextView2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ExploreActivity.this, MyVisaActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            }
        });

        mFooterTextView3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ExploreActivity.this, NewTripGenralActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

    }
}
