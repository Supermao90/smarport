package smartport.syaywa.com.smarport.NewTrip;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import smartport.syaywa.com.smarport.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wenshenm on 3/28/15.
 */
public class ApplicaitonProcessFragment extends BaseFragment
{
    private ViewPager mViewPager;
    private FragmentPagerAdapter mViewPagerAdapter;
    private LinearLayout mDotIndicator;
    private static final int PAGE_SIZE = 4;
    private List<ImageView> mImageDotsList = new ArrayList<ImageView>(PAGE_SIZE);
    private ImageView mDotUnselected;
    private ImageView mDotSelected;
    private LinearLayout.LayoutParams mImageViewLayoutParams;
    private int mCurrentIndex = 0;
    private boolean mIsCurrentDotSet = false;

    @Override
    public void onResume()
    {
        super.onResume();
        mCurrentIndex = mViewPager.getCurrentItem();

        mDotSelected = (ImageView) mDotIndicator.getChildAt(mCurrentIndex + 1);
        mDotSelected.setImageDrawable(getResources().getDrawable(R.drawable.dot1));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.layout_fragment_application_process, container, false);
        mViewPager = (ViewPager) v.findViewById(R.id.application_view_pager);
        mViewPagerAdapter = new ViewPagerAdapter(getActivity(), getChildFragmentManager());
        mViewPager.setAdapter(mViewPagerAdapter);
        mDotIndicator = (LinearLayout) v.findViewById(R.id.dot_page_indicator);
        mDotUnselected = (ImageView) v.findViewById(R.id.img_dot);
        mImageViewLayoutParams = (LinearLayout.LayoutParams) mDotUnselected.getLayoutParams();

        for (int i = 0; i < PAGE_SIZE; i++)
        {
            ImageView img = new ImageView(getActivity());
            img.setImageDrawable(mDotUnselected.getDrawable());
            img.setLayoutParams(mImageViewLayoutParams);
            mDotIndicator.addView(img);
        }

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                mDotSelected = (ImageView) mDotIndicator.getChildAt(mCurrentIndex + 1);
                mDotSelected.setImageDrawable(getResources().getDrawable(R.drawable.dot2));

                mCurrentIndex = mViewPager.getCurrentItem();

                mDotSelected = (ImageView) mDotIndicator.getChildAt(mCurrentIndex + 1);
                mDotSelected.setImageDrawable(getResources().getDrawable(R.drawable.dot1));
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });
        return v;
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private Context mContext = null;
        private ArrayList<String> mTitles = new ArrayList<String>();
        private ArrayList<String> mBody = new ArrayList<String>();

        public ViewPagerAdapter(Context ctxt, FragmentManager mgr)
        {
            super(mgr);
            mContext = ctxt;
            init();
        }

        private void init(){
            mTitles.add("准备护照和照片");
            mTitles.add("填写并打印DS-160表");
            mTitles.add("预约面签");
            mTitles.add("准备入境填写海关申报单");

            mBody.add("护照：有效期从美国离境时至少半年以上的护照原件，至少留有一整张空白页申请人护照原件\n" +
                    "\n" +
                    "照片：51X51毫米6个月之内彩色免冠照片2张，要求背景需为白色，且露出耳朵，准备电子版\n");
            mBody.add("提交表格之后，将无法进行任何更改，如需更改，需要重新创建填写新的DS-160表格。请牢记最后一份DS-160编号以便预约面谈时间。\n" +
                    "\n" +
                    "记住DS-160确认页中的条形码下面的以AA开头的确认号。\n");

            mBody.add("1）网上预约\n" +
                    "\n" +
                    "1- 打开网站www.ustraveldocs.com，选择在中国。\n" +
                    "\n" +
                    "2- 选择第一次申请签证，然后选择非移民签证。\n" +
                    "\n" +
                    "3- 如果以前未注册过请点击新用户注册，如果注册过请直接登录；\n" +
                    "\n" +
                    "4- 选择非移民，然后选择面谈- B2签证。\n" +
                    "\n" +
                    "5- 填写个人信息，注意此时需要DS-160确认页上的确认号。\n" +
                    "\n" +
                    "6- 选择取件的地点，可以选择自取或者邮寄；\n" +
                    "\n" +
                    "7- 选择付款方式\n" +
                    "\n" +
                    "申请人将可以选择使用任何在中国发行的借记卡在线支付。\n" +
                    "\n" +
                    "申请人也将能够在任何一台中信自动取款机上用银联ATM卡。\n" +
                    "\n" +
                    "申请人可以在在任何中信银行分支使用现金支付；\n" +
                    "\n" +
                    "非移民签证类型申请人只需支付160美元手续费，此外无需更多额外费用。\n" +
                    "\n" +
                    "2） 呼叫中心 电话\n" +
                    "\n" +
                    "4008-872-333 （全国通用号码） \n" +
                    "\n" +
                    "(021) 3881-4611 (上海本地号码) \n" +
                    "\n" +
                    "(86-21) 3881-4611 （境外拨打） \n" +
                    "\n" +
                    "无预约费，咨询费。\n");
            mBody.add("不可以携带易腐食物，包括水果，蔬菜，肉类或者农作物，不可以携带麻醉成分药品。");
        }

        @Override
        public int getCount()
        {
            return (PAGE_SIZE);
        }

        @Override
        public Fragment getItem(int position)
        {
            return ApplicationProcessPageFragment.newInstance(position, mTitles.get(position), mBody.get(position));
        }

        @Override
        public String getPageTitle(int position)
        {
            return (ApplicationProcessPageFragment.getTitle(mContext, position));
        }
    }
}
