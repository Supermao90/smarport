package smartport.syaywa.com.smarport.Receivers;

import android.content.Context;
import android.content.Intent;
import com.parse.ParsePushBroadcastReceiver;

/**
 * Created by wenshenm on 6/18/15.
 */
public class SmarportPushBroadcastReceiver extends ParsePushBroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        System.out.println("Wensheng on regular receive " + intent.getAction() + " " + intent.getExtras() + " " + intent.getData());
    }

    @Override
    protected void onPushDismiss(Context context, Intent intent)
    {
        System.out.println("Wensheng on push dismiss " + intent.getAction() + " " + intent.getExtras() + " " + intent.getData());
    }

    @Override
    protected void onPushOpen(Context context, Intent intent)
    {
        System.out.println("Wensheng on push open " + intent.getAction() + " " + intent.getExtras() + " " + intent.getData());
    }

    @Override
    protected void onPushReceive(Context context, Intent intent)
    {
        System.out.println("Wensheng on push receive " + intent.getAction() + " " + intent.getExtras() + " " + intent.getData());
    }
}
