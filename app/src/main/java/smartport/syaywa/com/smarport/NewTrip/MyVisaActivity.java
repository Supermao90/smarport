package smartport.syaywa.com.smarport.NewTrip;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import smartport.syaywa.com.smarport.R;

/**
 * Created by wenshenm on 3/15/15.
 */
public class MyVisaActivity extends Activity
{

    private static String URL = "URL";
    private static String WEB_VIEW = "WEB_VIEW";

    private int mFrameLayoutHeight;
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;

    private LinearLayout mFooterWidget0;
    private LinearLayout mFooterWidget1;
    private LinearLayout mFooterWidget2;
    private LinearLayout mFooterWidget3;
    private LinearLayout mFooterWidget4;

    private LinearLayout mFooter;
    private boolean mIsWebViewOn;

    private TextView mTitle;


    private EditText mInputSearch;

    private TextView mFooterTextView1;
    private TextView mFooterTextView2;
    private TextView mFooterTextView3;
    private ListView mListView;
    private ExploreListViewArrayAdapter mListViewAdapter;
    private SharedPreferences mPrf;

    @Override
    protected void onResume()
    {
//        getWindow().getDecorView()
//                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onResume();

        if (mFooterTextView1 != null)
        {
            mFooterTextView1.setSelected(false);
            mFooterTextView2.setSelected(true);
            mFooterTextView3.setSelected(false);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPrf = this.getSharedPreferences(this.getPackageName(), Context.MODE_PRIVATE);

        setContentView(R.layout.activity_explore);

        mInputSearch = (EditText) findViewById(R.id.explore_inputSearch);
        mInputSearch.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if (hasFocus)
                {
                    Intent intent = new Intent(MyVisaActivity.this, NewTripGenralActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra(ExploreActivity.SEARCH, true);
                    startActivity(intent);
                }
            }
        });

        mFooterTextView1 = (TextView) findViewById(R.id.footer_text_1);
        mFooterTextView2 = (TextView) findViewById(R.id.footer_text_2);
        mFooterTextView3 = (TextView) findViewById(R.id.footer_text_3);

        mFooterTextView2.setSelected(true);

        mFooterTextView1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mFooterTextView1.setSelected(false);
                v.setSelected(true);
                Intent intent = new Intent(MyVisaActivity.this, ExploreActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            }
        });

        mFooterTextView3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mFooterTextView1.setSelected(false);
                v.setSelected(true);
                Intent intent = new Intent(MyVisaActivity.this, NewTripGenralActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });
        mListView = (ListView) findViewById(R.id.explore_list_view);
        int visaApplicationStatus = 1;
        if (getIntent() != null && getIntent().getIntExtra(Util.APPLICATION_STATUS, 1) != 1)
        {
            visaApplicationStatus = getIntent().getIntExtra(Util.APPLICATION_STATUS, 1);
            mPrf.edit().putInt(Util.APPLICATION_STATUS, visaApplicationStatus).commit();
            System.out.println("Wensheng check 1 " +  visaApplicationStatus);
        }else{
            if(mPrf.getInt(Util.APPLICATION_STATUS, 1) != 1){
                visaApplicationStatus = mPrf.getInt(Util.APPLICATION_STATUS, 1);
                System.out.println("Wensheng check 2 " +  visaApplicationStatus);
            }
            System.out.println("Wensheng check 3 " +  visaApplicationStatus);
        }


        System.out.println("Wensheng check status " + visaApplicationStatus);
        mListViewAdapter =
                new ExploreListViewArrayAdapter(this, R.layout.explore_list_view_item, R.id.explore_country_name,
                        true, visaApplicationStatus);
        mListView.setAdapter(mListViewAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            Intent intent = new Intent();

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                if (ExploreListViewArrayAdapter.getMoreIndex() == position)
                {
                    intent.setClass(MyVisaActivity.this, NewTripGenralActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
            }
        });
        mInputSearch = (EditText) findViewById(R.id.explore_inputSearch);

        mInputSearch.addTextChangedListener(new TextWatcher()
        {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3)
            {
                // When user changed the Text
//                My.this.mListViewAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
            {
                Intent intent = new Intent(MyVisaActivity.this, NewTripGenralActivity.class);
                startActivity(intent);
            }

            @Override
            public void afterTextChanged(Editable s)
            {
//                if (s.toString().length() > 0)
//                {
//                    mInputSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//                }
//                else
//                {
//                    //Assign your image again to the view, otherwise it will always be gone even if the text is 0 again.
//                    mInputSearch.setCompoundDrawablesWithIntrinsicBounds(R.drawable.search_icon, 0, 0, 0);
//                }
            }
        });
    }
//        FrameLayout exploreFramLayout = (FrameLayout) findViewById(R.id.explore_framelayout);
//        mTitle = (TextView) findViewById(R.id.tv_header_title_my_visa);
//        mTitle.setText(getString(R.string.my_visa));
//
//        mFragmentManager = getFragmentManager();
//        mFragmentTransaction = mFragmentManager.beginTransaction();
//
//        Fragment fragment = new MyVisaSwipeViewFragment();
//
//        mFragmentTransaction.add(R.id.content_frame, fragment, "MY_VISA");
//        mFragmentTransaction.commit();
//
//        mFooter = (LinearLayout) findViewById(R.id.layout_footer_my_visa);
//
//        mFooterWidget0 = (LinearLayout) mFooter.getChildAt(0);
//        mFooterWidget1 = (LinearLayout) mFooter.getChildAt(1);
//        mFooterWidget2 = (LinearLayout) mFooter.getChildAt(2);
//        mFooterWidget3 = (LinearLayout) mFooter.getChildAt(3);
//        mFooterWidget4 = (LinearLayout) mFooter.getChildAt(4);
//
//        //0
//        TextView footerWidget0Title = (TextView) mFooterWidget0.findViewById(R.id.footer_widget_text_above);
//        footerWidget0Title.setText("6.26");
//
//        TextView footerWidget0Text = (TextView) mFooterWidget0.getChildAt(1);
//        footerWidget0Text.setText("Exchange Rate");
//
////        RelativeLayout openWebViewButton = (RelativeLayout) mFooterWidget0.findViewById(R.id.btn_open);
////        openWebViewButton.setOnClickListener(openWebviewButtonClickListener("http://qianzhengshequ.weebly.com/"));
//
//        getFragmentManager().addOnBackStackChangedListener(
//                new FragmentManager.OnBackStackChangedListener()
//                {
//                    public void onBackStackChanged()
//                    {
//                        System.out.println("Wensheng on back stack changed");
//                        Fragment webViewFragment = getFragmentManager().findFragmentByTag(WEB_VIEW);
//                        if (webViewFragment == null || webViewFragment.isDetached() || webViewFragment.isRemoving() ||
//                                !webViewFragment.isVisible())
//                        {
//                            if (mFooter.getVisibility() == View.GONE)
//                            {
//                                mFooter.setVisibility(View.VISIBLE);
//                            }
//                        }
//                    }
//                });
//
//
//        //1
//        TextView footerWidget1Title = (TextView) mFooterWidget1.findViewById(R.id.footer_widget_text_above);
//        footerWidget1Title.setText("$150");
//
//        TextView footerWidget1Text = (TextView) mFooterWidget1.getChildAt(1);
//        footerWidget1Text.setText("Total Fee");
//
//        //2
//        ImageView footerWidget2Title = (ImageView) mFooterWidget2.findViewById(R.id.footer_widget_image_above);
//        footerWidget2Title.setImageDrawable(getResources().getDrawable(R.drawable.common_full_open_on_phone));
//
//        TextView footerWidget2Text = (TextView) mFooterWidget2.getChildAt(1);
//        footerWidget2Text.setText("NEW TRIP");
//
//        RelativeLayout openWebViewButton2 = (RelativeLayout) mFooterWidget2.findViewById(R.id.btn_open);
//        openWebViewButton2.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                Intent intent = new Intent(MyVisaActivity.this, NewTripFragmentActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        //3
//        ImageView footerWidget3Title = (ImageView) mFooterWidget3.findViewById(R.id.footer_widget_image_above);
//        footerWidget3Title.setImageDrawable(getResources().getDrawable(R.drawable.snow));
//
//        TextView footerWidget3Text = (TextView) mFooterWidget3.getChildAt(1);
//        footerWidget3Text.setText("Climate Feature");
//
//        //4
//        ImageView footerWidget4Title = (ImageView) mFooterWidget4.findViewById(R.id.footer_widget_image_above);
//        footerWidget4Title.setImageDrawable(getResources().getDrawable(R.drawable.more));
//
//        TextView footerWidget4Text = (TextView) mFooterWidget4.getChildAt(1);
//        footerWidget4Text.setText("More");
//
//        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.content_frame);
//        ViewTreeObserver vto = frameLayout.getViewTreeObserver();
//        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
//        {
//            @Override
//            public void onGlobalLayout()
//            {
//                frameLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                mFrameLayoutHeight = frameLayout.getMeasuredHeight();
//            }
//        });
//
//    }
//
//    private View.OnClickListener openWebviewButtonClickListener(final String url)
//    {
//        View.OnClickListener listener = new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                mFooter.setVisibility(View.GONE);
//                WebViewFragment fragment = new WebViewFragment();
//                Bundle b = new Bundle();
//                b.putString(URL, url);
//                fragment.setArguments(b);
//                mFragmentTransaction = getFragmentManager().beginTransaction();
//                mFragmentTransaction.replace(R.id.content_frame, fragment, WEB_VIEW);
//                mFragmentTransaction.addToBackStack(WEB_VIEW);
//                mFragmentTransaction.commit();
//                mIsWebViewOn = true;
//            }
//        };
//        return listener;
//    }
//
//    @Override
//    public void onBackPressed()
//    {
//        if (mIsWebViewOn)
//        {
//            getFragmentManager().popBackStack();
//            return;
//        }
//
//        super.onBackPressed();
//    }
//
//    @Override
//    public void onMessageReceiveFromFragment(String message)
//    {
////        System.out.println("Wensheng fragment " + " sends message " + message);
//    }
}
