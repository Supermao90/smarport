package smartport.syaywa.com.smarport.NewTrip;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import smartport.syaywa.com.smarport.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by wenshenm on 3/25/15.
 */
public class PrepDocFragment extends BaseFragment
{

    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private SharedPreferences mPrf;

    @Override
    public void onResume()
    {
        super.onResume();
        listAdapter.invalid();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        mPrf = this.getActivity().getSharedPreferences(
                this.getActivity().getPackageName(), Context.MODE_PRIVATE);
        final ViewGroup rootViewDocPrep = (ViewGroup) inflater
                .inflate(R.layout.layout_fragment_docprep, container, false);

        expListView = (ExpandableListView) rootViewDocPrep.findViewById(R.id.lvExp);
        prepareListData();
        listAdapter = new ExpandableListAdapter(this.getActivity(), listDataHeader, listDataChild, true, mPrf);
        expListView.setAdapter(listAdapter);
        expListView.setIndicatorBoundsRelative(900, 1080);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener()
        {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id)
            {
//                Toast.makeText(
//                        PrepDocFragment.this.getActivity().getApplicationContext(),
//                        listDataHeader.get(groupPosition)
//                                + " : "
//                                + listDataChild.get(
//                                listDataHeader.get(groupPosition)).get(
//                                childPosition), Toast.LENGTH_SHORT)
//                        .show();
                return false;
            }
        });

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener()
        {

            @Override
            public void onGroupExpand(int groupPosition)
            {
//                Toast.makeText(PrepDocFragment.this.getActivity().getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Expanded",
//                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener()
        {

            @Override
            public void onGroupCollapse(int groupPosition)
            {
//                Toast.makeText(PrepDocFragment.this.getActivity().getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Collapsed",
//                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener()
        {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id)
            {
//                Toast.makeText(PrepDocFragment.this.getActivity().getApplicationContext(),
//                        "Group Clicked " + listDataHeader.get(groupPosition),
//                        Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        return rootViewDocPrep;

    }

    private void prepareListData()
    {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("有效护照");
        listDataHeader.add("照片");
        listDataHeader.add("非移民申请电子申请表(DS-160)");
        listDataHeader.add("身份证复印件");
        listDataHeader.add("户口本复印件");
        listDataHeader.add("资产证明");
        listDataHeader.add("在职证明");
        listDataHeader.add("机票酒店预订单");
        listDataHeader.add("行程单");

        List<String> ready = new ArrayList<String>();
        ready.add("请一定准备好");

        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("The Shawshank Redemption");
        top250.add("The Godfather");
        top250.add("The Godfather: Part II");
        top250.add("Pulp Fiction");
        top250.add("The Good, the Bad and the Ugly");
        top250.add("The Dark Knight");
        top250.add("12 Angry Men");

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("The Conjuring");
        nowShowing.add("Despicable Me 2");
        nowShowing.add("Turbo");
        nowShowing.add("Grown Ups 2");
        nowShowing.add("Red 2");
        nowShowing.add("The Wolverine");

        List<String> comingSoon = new ArrayList<String>();
        comingSoon.add("2 Guns");
        comingSoon.add("The Smurfs 2");
        comingSoon.add("The Spectacular Now");
        comingSoon.add("The Canyons");
        comingSoon.add("Europa Report");


        for(String item : listDataHeader){
            listDataChild.put(item, ready);
        }

//        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
//        listDataChild.put(listDataHeader.get(1), nowShowing);
//        listDataChild.put(listDataHeader.get(2), comingSoon);
    }
}
