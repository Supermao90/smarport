package smartport.syaywa.com.smarport.NewTrip;

import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import smartport.syaywa.com.smarport.R;

/**
 * Created by wenshenm on 3/29/15.
 */
public class WebViewFragment extends BaseFragment
{
    private static String URL = "URL";
    private static String FRAGMENT_ID = "FRAGMENT_ID";

//    /**
//     * Parses Html from a URI, returning a string that will display nicely in an Html view
//     * @param uri The URI to load
//     * @return A clean version of the html
//     * @throws java.io.IOException If there's a problem with the GET request from the URI.
//     */
//    public static String getDisplayFriendlyHtml(final URI uri) throws IOException
//    {
//        final HttpClient client = new DefaultHttpClient();
//        final HttpGet get = new HttpGet(uri);
//        final ResponseHandler<String> handler = new BasicResponseHandler();
//        final String html = client.execute(get, handler);
//        return getDisplayFriendlyHtml(html);
//    }
//
//    /**
//     * Parses Html, returning a string that will display nicely in an Html view
//     * @param html The HTML to parse
//     * @return A clean version of the html
//     */
//    public static String getDisplayFriendlyHtml(String html)
//    {
//        //Add CSS to hide elements we don't want, and style the ones we do
//        final String style = "<style>@font-face{font-family:HelveticaNueu;font-weight:normal;font-style:normal;font-variant:normal;}.a-divider,.cs-help-header,.header,.cs-help-breadcrumb,.cs-help-search-footer,.cs-help-sidebar,header,#rhf,#navFooter{display:none}body,tr,div{background-color:transparent !important;color:#fff;font-family:HelveticaNueu !important;}a,ul,ol,.a-list-item{color:#fff !important;}div.help-pop-up,.cs-help-content,.cs-help-container,body,td.amabot_center,div.amabot_center {margin:0px !important;padding:0px !important;}#wrapper,.cs-help-content,.cs-help-container{width:auto;}</style>";
//        html = html.replaceAll("<body.*?>","$0"+style);
//
//        return html;
//    }

    private WebView mWebView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.layout_web_view, container, false);

        Button back = (Button) v.findViewById(R.id.web_view_back);
        mWebView = (WebView) v.findViewById(R.id.web_view);
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        String URL = this.getArguments().getString("URL");
        mWebView.loadUrl(URL);

        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                FragmentManager fm = getActivity().getFragmentManager();
                fm.popBackStack();
            }
        });

        return v;
    }
}
