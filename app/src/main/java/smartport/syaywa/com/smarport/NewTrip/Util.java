package smartport.syaywa.com.smarport.NewTrip;

/**
 * Created by wenshenm on 4/1/15.
 */
public class Util
{
    public final static String FRAGMENT_ANIMATION_START = "FRAGMENT_ANIMATION_START";
    public final static String FRAGMENT_ANIMATION_END = "FRAGMENT_ANIMATION_END";
    public final static int FRAGMENT_TRANSIT_TIME = 500;
    public final static String DOCS_PREP_KEY = "DOCS_PREP_KEY";
    public final static String APPLICATION_STATUS = "APPLICATION_STATUS";

}
