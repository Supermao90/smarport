package smartport.syaywa.com.smarport.Earth;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MyGLSurfaceView extends GLSurfaceView
{
    GLRender myRenderer;
    private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
    private float mPreviousY;
    private float mPreviousX;

    public void toRoatX(float rox, float degree)
    {
        myRenderer.toRoatX(rox, degree);
    }

    public void toRoatY(float roy, float degree)
    {
        myRenderer.toRoatY(roy, degree);
    }

    public MyGLSurfaceView(Context context, AttributeSet as)
    {
        super(context);
        myRenderer = new GLRender(context);
        this.setRenderer(myRenderer);
        this.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        float x = e.getX();
        float y = e.getY();
        switch (e.getAction())
        {
            case MotionEvent.ACTION_MOVE:
                float dy = y - mPreviousY;
                float dx = x - mPreviousX;
                float yAngle = myRenderer.getRoatY();
                float xAngle = myRenderer.getRoatX();
                yAngle += dx * TOUCH_SCALE_FACTOR;
                xAngle += dy * TOUCH_SCALE_FACTOR;
                myRenderer.setRoatY(yAngle);
                myRenderer.setRoatX(xAngle);
                requestRender();
        }
        mPreviousY = y;
        mPreviousX = x;
        return true;
    }
}
