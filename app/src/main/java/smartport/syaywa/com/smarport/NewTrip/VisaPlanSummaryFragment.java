package smartport.syaywa.com.smarport.NewTrip;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import smartport.syaywa.com.smarport.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by wenshenm on 3/29/15.
 */
public class VisaPlanSummaryFragment extends BaseFragment
{
    private SharedPreferences mPrf;
    private List<String> listDataHeader;
    private boolean mIsfinished = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (listDataHeader == null)
        {
            listDataHeader = new ArrayList<String>();

            // Adding child data
            listDataHeader.add("有效护照");
            listDataHeader.add("照片");
            listDataHeader.add("非移民申请电子申请表(DS-160)");
            listDataHeader.add("身份证复印件");
            listDataHeader.add("户口本复印件");
            listDataHeader.add("资产证明");
            listDataHeader.add("在职证明");
            listDataHeader.add("机票酒店预订单");
            listDataHeader.add("行程单");
        }
        mPrf = this.getActivity().getSharedPreferences(
                this.getActivity().getPackageName(), Context.MODE_PRIVATE);
        View v = inflater.inflate(R.layout.layout_visa_plan_summary_framgnet, container, false);
        TextView textView = (TextView) v.findViewById(R.id.docs_remaining_tv);
        NumberPicker np = (NumberPicker) v.findViewById(R.id.item_picker);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        Set<String> docs = mPrf.getStringSet(Util.DOCS_PREP_KEY, new HashSet<String>());
        String[] resStr;
        int total = listDataHeader.size();
        int n = docs.size();
        if (n == total)
        {
            mIsfinished = true;
            resStr = new String[]{"所有文件都准备好了,干得不错伙计"};
        }
        else
        {
            mIsfinished = false;
            np.setMinValue(0);
            System.out.println("Wensheng n = " + n);
            np.setMaxValue(total - n - 1);
            textView.setText(String.format(getString(R.string.docs_remaining), total - n));
            resStr = new String[total - n];
        }

        int j = 0;
        for (int i = 0; i < listDataHeader.size(); i++)
        {
            String indexStr = "" + i;
            if (!docs.contains(indexStr))
            {
                resStr[j++] = listDataHeader.get(i);
            }
        }
        System.out.println("Wensheng check res " + Arrays.toString(resStr));
        np.setDisplayedValues(resStr);

        Button btnAddtoMyTrip = (Button) v.findViewById(R.id.add_to_calendar);
        btnAddtoMyTrip.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(VisaPlanSummaryFragment.this.getActivity(), MyVisaActivity.class);
                if (mIsfinished)
                {
                    i.putExtra(Util.APPLICATION_STATUS, 3);
                }
                else
                {
                    i.putExtra(Util.APPLICATION_STATUS, 2);
                }
                startActivity(i);
            }
        });

        return v;
    }
}
