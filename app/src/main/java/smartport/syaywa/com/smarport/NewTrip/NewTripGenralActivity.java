package smartport.syaywa.com.smarport.NewTrip;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import smartport.syaywa.com.smarport.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by wenshenm on 4/18/15.
 */
public class NewTripGenralActivity extends Activity
{
    private final static String TAG = NewTripGenralActivity.class.getSimpleName();
    private EditText mInputSearch;
    private TextView mFooterTextView1;
    private TextView mFooterTextView2;
    private TextView mFooterTextView3;

    private ArrayList<String> mItems;
    private IndexableListView mListView;
    private ContentAdapter mListViewAdapter;

    @Override
    protected void onResume()
    {
//        getWindow().getDecorView()
//                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onResume();

        if (mFooterTextView1 != null)
        {
            mFooterTextView1.setSelected(false);
            mFooterTextView2.setSelected(false);
            mFooterTextView3.setSelected(true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_explore);

        ListView viewNeedToHide1 = (ListView) findViewById(R.id.explore_list_view);
        viewNeedToHide1.setVisibility(View.GONE);

        mListView = (IndexableListView) findViewById(R.id.new_trip_index_list_view);
        mListView.setVisibility(View.VISIBLE);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            Intent intent = new Intent();
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                TextView tv = (TextView) view.findViewById(android.R.id.text1);
                if(tv.getText().equals("USA")){
                    intent.setClass(NewTripGenralActivity.this, NewTripFragmentActivity.class);
                    startActivity(intent);
                }
            }
        });

        mItems = new ArrayList<String>();
        InputStream inputStream = getResources().openRawResource(R.raw.countries);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream), 8192);

        try
        {
            String line;
            while ((line = reader.readLine()) != null)
            {
                mItems.add(line);
            }
        }
        catch (IOException e)
        {
            Log.e(TAG, "Error loading text");
            throw new RuntimeException(e);
        }
        Collections.sort(mItems);

        mListViewAdapter = new ContentAdapter(this,
                android.R.layout.simple_list_item_1, mItems);

        mListView.setAdapter(mListViewAdapter);
        mListView.setFastScrollEnabled(true);
        mListView.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState)
            {
                System.out.println("Wensheng changed " + scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
            {
                System.out.println("Wensheng onScroll " + firstVisibleItem + " " + " " +visibleItemCount + " " + totalItemCount);
            }
        });

        mInputSearch = (EditText) findViewById(R.id.explore_inputSearch);
        if (getIntent().getExtras() != null && getIntent().getBooleanExtra(ExploreActivity.SEARCH, false))
        {
            (new Handler()).postDelayed(new Runnable()
            {

                public void run()
                {
                    mInputSearch.dispatchTouchEvent(MotionEvent
                            .obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0,
                                    0, 0));
                    mInputSearch.dispatchTouchEvent(MotionEvent
                            .obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0,
                                    0));

                }
            }, 100);
        }

        mInputSearch.addTextChangedListener(new TextWatcher()
        {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3)
            {
                // When user changed the Text
                NewTripGenralActivity.this.mListViewAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().length() > 0)
                {
                    mInputSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
                else
                {
                    //Assign your image again to the view, otherwise it will always be gone even if the text is 0 again.
                    mInputSearch.setCompoundDrawablesWithIntrinsicBounds(R.drawable.search_icon, 0, 0, 0);
                }
            }
        });

        mFooterTextView1 = (TextView) findViewById(R.id.footer_text_1);
        mFooterTextView2 = (TextView) findViewById(R.id.footer_text_2);
        mFooterTextView3 = (TextView) findViewById(R.id.footer_text_3);

        mFooterTextView3.setSelected(true);

        mFooterTextView1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mFooterTextView1.setSelected(false);
                v.setSelected(true);
                Intent intent = new Intent(NewTripGenralActivity.this, ExploreActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            }
        });

        mFooterTextView2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mFooterTextView1.setSelected(false);
                v.setSelected(true);
                Intent intent = new Intent(NewTripGenralActivity.this, MyVisaActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });
    }

    private class ContentAdapter extends ArrayAdapter<String> implements SectionIndexer
    {
        private String mSections = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public ContentAdapter(Context context, int textViewResourceId,
                              List<String> objects)
        {
            super(context, textViewResourceId, objects);
        }

        @Override
        public int getPositionForSection(int section)
        {
//            If there is no item for current section, previous section will be selected
            for (int i = section; i >= 0; i--)
            {
                for (int j = 0; j < getCount(); j++)
                {
                    if (i == 0)
                    {
                        // For numeric section
                        for (int k = 0; k <= 9; k++)
                        {
                            if (StringMatcher.match(String.valueOf(getItem(j).charAt(0)), String.valueOf(k)))
                            {
                                return j;
                            }
                        }
                    }
                    else
                    {
                        if (StringMatcher
                                .match(String.valueOf(getItem(j).charAt(0)), String.valueOf(mSections.charAt(i))))
                        {
                            return j;
                        }
                    }
                }
            }
            return 0;
        }

        @Override
        public int getSectionForPosition(int position)
        {
            return 0;
        }

        @Override
        public Object[] getSections()
        {
            String[] sections = new String[mSections.length()];
            for (int i = 0; i < mSections.length(); i++)
            {
                sections[i] = String.valueOf(mSections.charAt(i));
            }
            return sections;
        }
    }

}
