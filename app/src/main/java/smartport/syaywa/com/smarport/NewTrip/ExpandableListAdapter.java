package smartport.syaywa.com.smarport.NewTrip;

/**
 * Created by wenshenm on 3/22/15.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import smartport.syaywa.com.smarport.R;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ExpandableListAdapter extends BaseExpandableListAdapter
{

    private Context mContext;
    private List<String> mListDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> mListDataChild;
    private boolean mEnableCheckBox = false;
    private SharedPreferences mPrf;
    private Set<String> mInitCheckBoxIndexes;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData, boolean checkBox, SharedPreferences prf)
    {
        mContext = context;
        mListDataHeader = listDataHeader;
        mListDataChild = listChildData;
        mEnableCheckBox = checkBox;
        mPrf = prf;
        if(mPrf != null){
            mInitCheckBoxIndexes = mPrf.getStringSet(Util.DOCS_PREP_KEY, new HashSet<String>());
        }
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon)
    {
        return mListDataChild.get(mListDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent)
    {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null)
        {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
        return this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition)
    {
        return this.mListDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount()
    {
        return this.mListDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition)
    {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent)
    {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null)
        {
            LayoutInflater infalInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        final CheckBox checkbox = (CheckBox) convertView.findViewById(R.id.expandabler_list_checkbox);
        if(mInitCheckBoxIndexes != null && mInitCheckBoxIndexes.contains("" + groupPosition)){
            checkbox.setChecked(true);
        }
        final LinearLayout checkboxLayout =
                (LinearLayout) convertView.findViewById(R.id.expandabler_list_checkbox_layout);
        lblListHeader.setText(headerTitle);
//        checkboxLayout.setBackgroundColor(mContext.getResources().getColor(R.color.wallet_bright_foreground_holo_light));
        checkboxLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                Set<String> docs = mPrf.getStringSet(Util.DOCS_PREP_KEY, new HashSet<String>());
//                System.out.println("Wensheng get docs on click");
//                if (checkbox.isChecked())
//                {
//                    System.out.println("Wensheng about to remove  " + groupPosition);
//                    docs.remove("" + groupPosition);
//                    mPrf.edit().putStringSet(Util.DOCS_PREP_KEY, docs).commit();
//                }
//                else
//                {
//                    System.out.println("Wensheng about to add  " + groupPosition);
//                    docs.add("" + groupPosition);
//                    mPrf.edit().putStringSet(Util.DOCS_PREP_KEY, docs).commit();
//                }
                checkbox.performClick();
//                System.out.println(
//                        "Wensheng check after click " + mPrf.getStringSet(Util.DOCS_PREP_KEY, new HashSet<String>()));
            }
        });
        checkbox.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String position = "" + groupPosition;
                Set<String> docs = mPrf.getStringSet(Util.DOCS_PREP_KEY, new HashSet<String>());
                System.out.println("Wensheng get docs on click");
                if (docs.contains(position))
                {
                    System.out.println("Wensheng about to remove  " + groupPosition);
                    docs.remove(position);
                    mPrf.edit().putStringSet(Util.DOCS_PREP_KEY, docs).commit();
                }
                else
                {
                    System.out.println("Wensheng about to add  " + groupPosition);
                    docs.add(position);
                    mPrf.edit().putStringSet(Util.DOCS_PREP_KEY, docs).commit();
                }
                System.out.println(
                        "Wensheng check after click " + mPrf.getStringSet(Util.DOCS_PREP_KEY, new HashSet<String>()));
            }
        });
        ImageView pin = (ImageView) convertView.findViewById(R.id.pin);
        if (mEnableCheckBox)
        {
            checkbox.setVisibility(View.VISIBLE);
        }
        else
        {
            pin.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    public void invalid(){

    }

    @Override
    public boolean hasStableIds()
    {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
        return true;
    }
}