package smartport.syaywa.com.smarport.NewTrip;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import smartport.syaywa.com.smarport.R;

/**
 * Created by wenshenm on 4/17/15.
 */
public class ExploreListViewBaseAdapter extends BaseAdapter
{
    public static final String products[] = {"Italy", "USA", "Thailand"};
    public static int[] productsDrableSrc =
            {R.drawable.explore_italy, R.drawable.explore_usa, R.drawable.explore_thailand};

    Context context;
    String[] data;
    private static LayoutInflater inflater = null;

    public ExploreListViewBaseAdapter(Context context, String[] data)
    {
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return data.length;
    }

    @Override
    public Object getItem(int position)
    {
        return data[position];
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View vi = convertView;
        if (vi == null)
        {
//            vi = inflater.inflate(R.layout.row, null);
        }
//        TextView text = (TextView) vi.findViewById(R.id.text);
//        text.setText(data[position]);
        return vi;
    }
}