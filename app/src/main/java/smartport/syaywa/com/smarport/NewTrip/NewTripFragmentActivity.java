package smartport.syaywa.com.smarport.NewTrip;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import smartport.syaywa.com.smarport.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by wenshenm on 3/15/15.
 */
public class NewTripFragmentActivity extends Activity implements BaseFragment.OnFragmentInteractionListener
{

    private static String URL = "URL";
    private static String WEB_VIEW = "WEB_VIEW";
    private int mFrameLayoutHeight;

    private Button mButtonPrevious;
    private Button mButtonNext;
    private FragmentTransaction mFragmentTransaction;
    private List<Fragment> mFragments;
    private int mFragmentIndex;

    private TextView mFooterWidget0;
    private TextView mFooterWidget1;
    private TextView mFooterWidget2;
    private TextView mFooterWidget3;
    private TextView mFooterWidget4;
    private List<TextView> mFooterWidgets;

    private RelativeLayout mFooter;
    private boolean mIsWebViewOn;
    private TextView mFooterTextN1;

    private List<String> mTitles;
    private TextView mTitle;
    private Button mBtnBack;
    private boolean mIsFragmentInTransit;
    private View.OnClickListener mNextOnclickListener;

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    protected void onResume()
    {
//        getWindow().getDecorView()
//                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_trip);
        buildTitle();
        if (getIntent().getBooleanExtra(Util.DOCS_PREP_KEY, false))
        {
            mFragmentIndex = 1;
        }
        mTitle = (TextView) findViewById(R.id.tv_header_title);
        mTitle.setText(mTitles.get(mFragmentIndex));
        mBtnBack = (Button) findViewById(R.id.btn_fragment_back_top_left);
        mBtnBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(NewTripFragmentActivity.this, MyVisaActivity.class);
                startActivity(intent);
            }
        });

        if (mFragments == null)
        {
            mFragments = new ArrayList<Fragment>();
            Fragment overviewFragment = new OverviewFragment();
            Fragment prepDocFragment = new PrepDocFragment();
            Fragment applicationProcessFrgment = new ApplicaitonProcessFragment();
            Fragment ambassyLocationFragment = new AmbassyLocationFragment();
            Fragment visaPlanSummaryFragment = new VisaPlanSummaryFragment();

            mFragments.add(overviewFragment);
            mFragments.add(prepDocFragment);
            mFragments.add(applicationProcessFrgment);
            mFragments.add(ambassyLocationFragment);
            mFragments.add(visaPlanSummaryFragment);
        }

        mFragmentTransaction = getFragmentManager().beginTransaction();
        mFragmentTransaction.add(R.id.content_frame, mFragments.get(mFragmentIndex), "overview");
        mFragmentTransaction.commit();

//        mButtonPrevious = (Button) findViewById(R.id.btn_overview_previous);
//        mButtonNext = (Button) findViewById(R.id.btn_overview_next);
//        System.out.println("Wensheng fragment size " + mFragments.size());
//        mNextOnclickListener = new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                if (mFragmentIndex + 1 >= mFragments.size() || mIsFragmentInTransit)
//                {
//                    return;
//                }
//                mIsFragmentInTransit = true;
//                mButtonNext.setEnabled(!mIsFragmentInTransit);
//                mButtonPrevious.setEnabled(!mIsFragmentInTransit);
//                mFragmentTransaction = getFragmentManager().beginTransaction();
//                mFragmentTransaction.setCustomAnimations(R.anim.slide_in_bot, R.anim.slide_out_top, R.anim.slide_in_top,
//                        R.anim.slide_out_bot);
//                mFragmentTransaction.addToBackStack(null);
//                mFragmentTransaction.replace(R.id.content_frame, mFragments.get(++mFragmentIndex));
//                mFragmentTransaction.commit();
//                flipBackAfterAnimation();
//                hideRevealButtons();
//            }
//        };
//        mButtonNext.setOnClickListener(mNextOnclickListener);
//
//        mButtonPrevious.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                if (mFragmentIndex <= 0)
//                {
//                    return;
//                }
//                mIsFragmentInTransit = true;
//                mButtonNext.setEnabled(!mIsFragmentInTransit);
//                mButtonPrevious.setEnabled(!mIsFragmentInTransit);
//                FragmentManager fm = NewTripFragmentActivity.this.getFragmentManager();
//                fm.popBackStack();
//                mFragmentIndex--;
//                flipBackAfterAnimation();
//                hideRevealButtons();
//            }
//        });
//
        mFooter = (RelativeLayout) findViewById(R.id.layout_footer_nt);

        mFooter.getChildAt(mFragmentIndex).setSelected(true);

        mFooterWidget0 = (TextView) mFooter.getChildAt(0);
        mFooterWidget1 = (TextView) mFooter.getChildAt(1);
        mFooterWidget2 = (TextView) mFooter.getChildAt(2);
        mFooterWidget3 = (TextView) mFooter.getChildAt(3);
        mFooterWidget4 = (TextView) mFooter.getChildAt(4);

        mFooterWidgets = new ArrayList<TextView>();
        mFooterWidgets.add(mFooterWidget0);
        mFooterWidgets.add(mFooterWidget1);
        mFooterWidgets.add(mFooterWidget2);
        mFooterWidgets.add(mFooterWidget3);
        mFooterWidgets.add(mFooterWidget4);


        View.OnClickListener footerBtnListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Fragment pre = mFragments.get(mFragmentIndex);
                mFooterWidgets.get(mFragmentIndex).setSelected(false);
                mFragmentIndex = mFooter.indexOfChild(v);
                mIsFragmentInTransit = true;
                mFragmentTransaction = getFragmentManager().beginTransaction();
                mFragmentTransaction.addToBackStack(null);
                Fragment fragmentAboutToAdd = mFragments.get(mFragmentIndex);
                if(!fragmentAboutToAdd.isAdded()){
                    mFragmentTransaction.add(R.id.content_frame, fragmentAboutToAdd);
                }else{
                    for(Fragment frag : mFragments){
                        if(frag.isVisible()) mFragmentTransaction.hide(frag);
                    }
                    mFragmentTransaction.show(fragmentAboutToAdd);
                }
                mFragmentTransaction.commit();
                mTitle.setText(mTitles.get(mFragmentIndex));
                mFooterWidgets.get(mFragmentIndex).setSelected(true);
            }
        };

        mFooterWidget0.setOnClickListener(footerBtnListener);
        mFooterWidget1.setOnClickListener(footerBtnListener);
        mFooterWidget2.setOnClickListener(footerBtnListener);
        mFooterWidget3.setOnClickListener(footerBtnListener);
        mFooterWidget4.setOnClickListener(footerBtnListener);
//
//        //0
//        TextView footerWidget0Title = (TextView) mFooterWidget0.findViewById(R.id.footer_widget_text_above);
//        footerWidget0Title.setText("6.26");
//
//        TextView footerWidget0Text = (TextView) mFooterWidget0.getChildAt(1);
//        footerWidget0Text.setText("Exchange Rate");
//
//        RelativeLayout openWebViewButton = (RelativeLayout) mFooterWidget0.findViewById(R.id.btn_open);
//        openWebViewButton.setOnClickListener(openWebviewButtonClickListener("http://qianzhengshequ.weebly.com/"));
//
//        getFragmentManager().addOnBackStackChangedListener(
//                new FragmentManager.OnBackStackChangedListener()
//                {
//                    public void onBackStackChanged()
//                    {
//                        System.out.println("Wensheng on back stack changed");
//                        Fragment webViewFragment = getFragmentManager().findFragmentByTag(WEB_VIEW);
//                        if (webViewFragment == null || webViewFragment.isDetached() || webViewFragment.isRemoving() ||
//                                !webViewFragment.isVisible())
//                        {
//                            if (mFooter.getVisibility() == View.GONE)
//                            {
//                                mButtonPrevious.setVisibility(View.VISIBLE);
//                                mButtonNext.setVisibility(View.VISIBLE);
//                                mFooter.setVisibility(View.VISIBLE);
//                            }
//                        }
//                    }
//                });
//
//
//        //1
//        TextView footerWidget1Title = (TextView) mFooterWidget1.findViewById(R.id.footer_widget_text_above);
//        footerWidget1Title.setText("$150");
//
//        TextView footerWidget1Text = (TextView) mFooterWidget1.getChildAt(1);
//        footerWidget1Text.setText("Total Fee");
//
//        //2
//        ImageView footerWidget2Title = (ImageView) mFooterWidget2.findViewById(R.id.footer_widget_image_above);
//        footerWidget2Title.setImageDrawable(getResources().getDrawable(R.drawable.bag));
//
//        TextView footerWidget2Text = (TextView) mFooterWidget2.getChildAt(1);
//        footerWidget2Text.setText("New Trip");
//
//        //3
//        ImageView footerWidget3Title = (ImageView) mFooterWidget3.findViewById(R.id.footer_widget_image_above);
//        footerWidget3Title.setImageDrawable(getResources().getDrawable(R.drawable.snow));
//
//        TextView footerWidget3Text = (TextView) mFooterWidget3.getChildAt(1);
//        footerWidget3Text.setText("Climate Feature");
//
//        //4
//        ImageView footerWidget4Title = (ImageView) mFooterWidget4.findViewById(R.id.footer_widget_image_above);
//        footerWidget4Title.setImageDrawable(getResources().getDrawable(R.drawable.more));
//
//        TextView footerWidget4Text = (TextView) mFooterWidget4.getChildAt(1);
//        footerWidget4Text.setText("More");
//
//        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.content_frame);
//        ViewTreeObserver vto = frameLayout.getViewTreeObserver();
//        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
//        {
//            @Override
//            public void onGlobalLayout()
//            {
//                frameLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                mFrameLayoutHeight = frameLayout.getMeasuredHeight();
//            }
//        });
//
//        hideRevealButtons();
    }

    public void hideFooterAndBack()
    {
        mFooter.setVisibility(View.GONE);
        mBtnBack.setVisibility(View.GONE);
    }

    public void revealFooterAndBack()
    {
        mFooter.setVisibility(View.VISIBLE);
        mBtnBack.setVisibility(View.GONE);
    }

    private View.OnClickListener openWebviewButtonClickListener(final String url)
    {
        View.OnClickListener listener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mButtonPrevious.setVisibility(View.GONE);
                mButtonNext.setVisibility(View.GONE);
                mFooter.setVisibility(View.GONE);
                WebViewFragment fragment = new WebViewFragment();
                Bundle b = new Bundle();
                b.putString(URL, url);
                fragment.setArguments(b);
                mFragmentTransaction = getFragmentManager().beginTransaction();
                mFragmentTransaction.replace(R.id.content_frame, fragment, WEB_VIEW);
                mFragmentTransaction.addToBackStack(WEB_VIEW);
                mFragmentTransaction.commit();
                mIsWebViewOn = true;
            }
        };
        return listener;
    }

    @Override
    public void onBackPressed()
    {
        mFooter.setVisibility(View.VISIBLE);
        mBtnBack.setVisibility(View.VISIBLE);
        super.onBackPressed();
    }

    @Override
    public void onMessageReceiveFromFragment(String message)
    {
//        System.out.println("Wensheng fragment " + " sends message " + message);
    }

    private void flipBackAfterAnimation()
    {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                mIsFragmentInTransit = false;
                mButtonNext.setEnabled(!mIsFragmentInTransit);
                mButtonPrevious.setEnabled(!mIsFragmentInTransit);
                mTitle.setText(mTitles.get(mFragmentIndex));
            }
        }, Util.FRAGMENT_TRANSIT_TIME);
    }

    private void buildTitle()
    {
        mTitles = new ArrayList<String>();
        mTitles.add(getResources().getString(R.string.visa_overview));
        mTitles.add(getResources().getString(R.string.doc_prep));
        mTitles.add(getResources().getString(R.string.application_processs));
        mTitles.add(getResources().getString(R.string.ambassy_location));
        mTitles.add(getResources().getString(R.string.visa_plan_summary));
    }

    private void hideRevealButtons()
    {
        System.out.println("Wensheng check step " + mFragmentIndex);
        if (mFragmentIndex == 0)
        {
            mButtonPrevious.setVisibility(View.GONE);
        }
        else
        {
            if (mButtonPrevious.getVisibility() != View.VISIBLE)
            {
                mButtonPrevious.setVisibility(View.VISIBLE);
            }
        }
        if (mFragmentIndex + 1 == mFragments.size())
        {
            mButtonNext.setText(getString(R.string.view_my_visa));
            mButtonNext.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(NewTripFragmentActivity.this, MyVisaActivity.class);
                    startActivity(intent);
                }
            });
        }
        else
        {
            if (mButtonNext.getText() != getString(R.string.next_step))
            {
                mButtonNext.setText(getString(R.string.next_step));
                mButtonNext.setOnClickListener(mNextOnclickListener);
            }
        }
    }
}
