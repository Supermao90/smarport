package smartport.syaywa.com.smarport.NewTrip;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import smartport.syaywa.com.smarport.R;

/**
 * Created by wenshenm on 3/28/15.
 */

public class ApplicationProcessPageFragment extends Fragment
{
    private static final String KEY_POSITION = "position";
    private static final String TITLE = "title";
    private static final String BODY = "body";

    public static ApplicationProcessPageFragment newInstance(int position, String title, String body)
    {
        ApplicationProcessPageFragment frag = new ApplicationProcessPageFragment();
        Bundle args = new Bundle();

        args.putInt(KEY_POSITION, position);
        args.putString(TITLE, title);
        args.putString(BODY, body);
        frag.setArguments(args);
        return (frag);
    }

    public static String getTitle(Context context, int position)
    {
        return "" + (position + 1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.layout_application_process_page, container, false);
        final TextView pageIndicator = (TextView) rootView.findViewById(R.id.tv_page_number);
        pageIndicator.setText("" + (getArguments().getInt(KEY_POSITION, -1) + 1));
        final TextView title = (TextView) rootView.findViewById(R.id.process_title);
        title.setText(getArguments().getString(TITLE));
        final TextView body = (TextView) rootView.findViewById(R.id.process_body);
        body.setText(getArguments().getString(BODY));

        return rootView;
    }
}