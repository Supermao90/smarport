# README #

APK location:
app/build/outputs/apk/app-debug.apk or
https://bitbucket.org/smarport/smarport/src/26620d557739bd909e390b843a7e63c794802236/app/build/outputs/apk/?at=master

*This app is developed based on Nexus 5, has not fully tested on other majority mobile devices.

### What is this repository for? ###

Smarport is short for "Smart" and "Passport". 

This project is to help people that enjoy traveling abroad
but are lack of time or energy figuring out what should be prepared.
Also because of the mass of unprofessional travel agencies market,
people are afraid of things like tour scam or rip-offs so they rather do it
themselves.

More importantly, I would like to encourage more people to throw away
all kinds of excuses and just go to different places before it's too late.
Life is short, it's time to create some unforgettable memories with people
you love! 

### How do I get set up? ###

This project is developed with Android Studio. Simply import this project and run.

Or if you want to build with command
./gradlew assembleDebug

or build + install with command
./gradlew installDebug

### Who do I talk to? ###

Wensheng Mao

maowensheng@hotmail.com

617-653-2889